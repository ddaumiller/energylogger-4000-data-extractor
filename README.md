# Read Data from an energy logger 4000 


## rough idea of what this should do

* read the binary files that energy logger 4000 writes to its SD-card
* transform them to some easier data format so we can send it somewhere
* send the data to some kind of a database/UI
* visualize our energy consumption in a nice way
* teach me how to write small programs in rust ;-)



# Command Line Interface
Idea: calling the cli to parse the binary files into one output format.
```
eventlogger *.BIN -o json > events.json
```