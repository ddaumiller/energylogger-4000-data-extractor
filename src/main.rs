
use clap::Parser;
use csv::Writer;
use energylogger_lib::{DataSample, read_binary_file, get_samples_from_file_content};
use glob::glob_with;

use std::{
    fs::File,
    io::{self, Write},
    path::Path,
};



/// Search for a pattern in a file and display the lines that contain it.
#[derive(Parser)]
struct Cli {
    #[arg(short = 'o', long = "output", default_value_t = ("output/fullData.csv").to_string())]
    /// The path to the directory to write to
    outputpath: String,
}

fn main() -> io::Result<()> {
    let args = Cli::parse();

    println!("outputpath: {:?}", args.outputpath);

    let outputpath = Path::new(&args.outputpath);
    // outputpath.push("output");
    // outputpath.push("fullData");
    // outputpath.set_extension("csv");
    let mut result_vec: Vec<DataSample> = Vec::new();

    for datafile in get_datafile_names("input").unwrap() {
        if let Ok(path) = datafile {
            result_vec.extend(
                get_samples_from_file_content(read_binary_file(&path))
                    .expect(&(format!("could not get samples from file {}", path.display()))),
            );

            // let content = data.iter().fold("".to_string(), |acc: String, sample| {
            //     acc + &format_sample_json(sample) + ","
            // });
            /*             println!(
                "{:?}", content
            ); */
            // match write_text_file(&outputpath, &content) {
            //     Err(_) => panic!("couldn't write to {}!", outputpath.display()),
            //     Ok(_) => println!("processed {}!", outputpath.display()),
            // }
        }
    }
    write_samples_to_csv_file(result_vec, outputpath)?;

    Ok(())
}

fn get_datafile_names(dir: &str) -> Result<glob::Paths, glob::PatternError> {
    let options = glob::MatchOptions {
        case_sensitive: false,
        require_literal_separator: false,
        require_literal_leading_dot: false,
    };

    let glob_pattern = dir.to_owned() + "/*.BIN";

    glob_with(&glob_pattern, options)
}

fn write_samples_to_csv_file(input_list: Vec<DataSample>, path: &Path) -> io::Result<()> {
    // let display = path.display();
    // let file = match File::create(&path) {
    //     Err(why) => panic!("can't create {}: {}", display, why),
    //     Ok(file) => file,
    // };
    let mut csv_writer = Writer::from_path(&path)?;

    for input in input_list {
        // csv_writer.write_record(&[input.timestamp.format("%Y-%m-%d %H:%M:%S").to_string(),
        //                         voltage.to_string(),
        //                         current.to_string(),
        //                         (current * voltage).to_string(),
        //                         input.powerfactor_percent.to_string()])?;
        csv_writer.serialize(input)?;
    }
    csv_writer.flush()?;
    Ok(())
}

fn _write_text_file(path: &Path, content: &str) -> io::Result<()> {
    let display = path.display();

    let mut file = match File::create(&path) {
        Err(why) => panic!("can't create {}: {}", display, why),
        Ok(file) => file,
    };

    match file.write_all(content.as_bytes()) {
        Err(_) => panic!("couldn't write to {}!", display),
        Ok(_) => Ok(()),
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use chrono::NaiveDateTime;
    use tempfile::NamedTempFile;

    #[test]
    fn can_call_function() {
        let file_bytes = read_binary_file("test/B08CB78A.BIN");
        let first = file_bytes[0];
        assert_eq!(first, 0xe0);
    }

    #[test]
    fn can_enumerate_folder_of_binfiles() {
        let filenames: glob::Paths = get_datafile_names("test").unwrap();

        assert_eq!(filenames.enumerate().fold(0, |_acc, i| i.0 + 1), 2); //FIXME: this looks overly complicated to get the soze of a glob...
    }

    #[test]
    fn can_export_data_to_csv() {
        let data = get_samples_from_file_content(read_binary_file("test/B0618B3A.BIN")).unwrap();
        let output_file = NamedTempFile::new().expect("couldn't create tempFile");

        write_samples_to_csv_file(data[0..5].to_vec(), &output_file.path()).unwrap();
    }

}
