use chrono::{NaiveDate, NaiveDateTime, Duration};
use serde::Serialize;
use std::{
    fs::File,
    io::Read,
    path::Path,
};
use json::object;



// size information from http://wiki.td-er.nl/index.php?title=Energy_Logger_3500
const _HEADERBYTES: u8 = 3;
const _TIMESTAMPBYTES: u8 = 5;
const _SAMPLEBYTES: u8 = 5;
const _MAXFILESIZEBYTES: u16 = 10564;
const _MAXSAMPLESPERFILE: u16 = 2110;
pub const FILEENDING: [u8; 4] = [0xff, 0xff, 0xff, 0xff];

#[derive(Debug, PartialEq)]
pub enum FileType {
    Data,
    Info,
    // Setup, // Not used yet
}

#[derive(Debug, Clone, PartialEq, Serialize)]
pub struct DataSample {
    timestamp: NaiveDateTime,
    //voltage_decivolts: u32,
    voltage: f32,
    //current_milliamps: u32,
    current: f32,
    kilo_watt_hours: f32,
    powerfactor_percent: u8,
}
impl Default for DataSample {
    fn default() -> DataSample {
        DataSample {
            timestamp: NaiveDateTime::parse_from_str("2021-01-01 01:02:03", "%Y-%m-%d %H:%M:%S")
                .unwrap(),
            //voltage_decivolts: 0,
            voltage: 0.0,
            //current_milliamps: 0,
            current: 0.0,
            kilo_watt_hours: 0.0,
            powerfactor_percent: 0,
        }
    }
}

pub fn get_timestamp(sample: Vec<u8>) -> NaiveDateTime {
    /*  month = byte(0)
    day = byte(1)
    year = 2000 + byte(2)
    hour = byte(3)
    Minute = byte(4) */
    let month: u32 = sample[0].into();
    let day: u32 = sample[1].into();
    let baseyear: i32 = 2000;
    let offsetyear: i32 = sample[2].into();
    let year: i32 = baseyear + offsetyear;
    let hour: u32 = sample[3].into();
    let minute: u32 = sample[4].into();
    assert!(
        month < 13,
        "assertion failed for month with {}-{}-{}_{}:{}",
        year,
        month,
        day,
        hour,
        minute
    );
    assert!(
        day < 32,
        "assertion failed for day with {}-{}-{}_{}:{}",
        year,
        month,
        day,
        hour,
        minute
    );
    assert!(
        year > 1969,
        "assertion failed for year with {}-{}-{}_{}:{}",
        year,
        month,
        day,
        hour,
        minute
    );
    assert!(
        hour < 25,
        "assertion failed for hour with {}-{}-{}_{}:{}",
        year,
        month,
        day,
        hour,
        minute
    );
    assert!(
        minute < 61,
        "assertion failed for minute with {}-{}-{}_{}:{}",
        year,
        month,
        day,
        hour,
        minute
    );

    let date_time: NaiveDateTime = NaiveDate::from_ymd_opt(year, month, day)
        .expect("not a valid date")
        .and_hms_opt(hour, minute, 0)
        .expect("not a valid time");

    return date_time;
    //format("%a %b %e %T %Y").to_string();
}


pub fn get_filetype(file_content: Vec<u8>) -> Result<FileType, ()> {
    let fileheader = file_content[0..3].to_vec();
    if [0xe0, 0xc5, 0xea].to_vec() == fileheader {
        Ok(FileType::Data)
    } else if [0x49, 0x4e, 0x46].to_vec() == fileheader {
        Ok(FileType::Info)
    } else {
        Err(())
    }
}

fn _format_sample_json(input: &DataSample) -> String {
    //let voltage: f32 = input.voltage_decivolts as f32 / 10.0;
    //let current: f32 = input.current_milliamps as f32 / 1000.0;
    return json::stringify(object! {
        "timestamp" => input.timestamp.format("%Y-%m-%d %H:%M:%S").to_string(),
        "voltage" => input.voltage.to_string(),
        "current" => input.current.to_string(),
        "power" => (input.current * input.voltage).to_string(),
        "powerfactor" => input.powerfactor_percent.to_string(),
    });
}

pub fn read_binary_file(file_path: impl AsRef<Path>) -> Vec<u8> {
    match File::open(file_path) {
        Ok(mut file) => {
            let mut content_buffer = Vec::new();
            file.read_to_end(&mut content_buffer).unwrap();
            return content_buffer;
        }
        Err(error) => {
            panic!("couldn't open file: {}", error);
        }
    }
}

pub fn get_samples_from_file_content(content: Vec<u8>) -> Result<Vec<DataSample>, ()> {
    let header_raw = content[0..3].to_vec();
    let base_timestamp_raw = content[3..8].to_vec();
    let data_samples_raw = content[8..].to_vec();
    let samples_vec = data_samples_raw.chunks(5);
    let mut result_vec = Vec::new();
    match get_filetype(header_raw) {
        Ok(FileType::Data) => {
            let mut minutes_counter: i64 = 0;
            let base_time_stamp = get_timestamp(base_timestamp_raw);
            for sample in samples_vec {
                if sample.len() < 5 {
                    break;
                }
                let lastfoursampleparts: Vec<u8> = sample[1..=4].to_vec();
                if lastfoursampleparts == FILEENDING.to_vec() {
                    break;
                }

                minutes_counter += 1;
                let sample_timestamp = base_time_stamp + Duration::minutes(minutes_counter);
                let voltage = (sample[0] as u32 * 256 + sample[1] as u32) as f32 / 10.0;
                let current = (sample[2] as u32 * 256 + sample[3] as u32) as f32 / 1000.0;
                let energy = voltage * current * 60.0 / 1000.0;
                result_vec.push(DataSample {
                    timestamp: sample_timestamp,
                    //voltage_decivolts: sample[0] as u32 * 256 + sample[1] as u32,
                    voltage: voltage,
                    //current_milliamps: sample[2] as u32 * 256 + sample[3] as u32,
                    current: current,
                    kilo_watt_hours: energy,
                    powerfactor_percent: sample[4],
                })
            }
            Ok(result_vec)
        }
        Ok(FileType::Info) => Ok(result_vec),
        // Ok(FileType::Setup) => Ok(result_vec),
        Err(_) => Err(()),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::NaiveDateTime;
    

    #[test]
    fn can_verify_datafile_header() {
        let file_bytes = read_binary_file("test/B0618B3A.BIN");
        //let file_bytes = read_binary_file("test/B08CB78A.BIN");
        let result = get_filetype(file_bytes).unwrap();
        assert_ne!(result, FileType::Info);
        // assert_ne!(result, FileType::Setup)
    }
    #[test]
    fn can_verify_infofile_header() {
        let file_bytes = read_binary_file("test/A081B56A.BIN_info");
        //let file_bytes = read_binary_file("test/B08CB78A.BIN");
        let result = get_filetype(file_bytes).unwrap();
        // assert_ne!(result, FileType::Setup);
        assert_ne!(result, FileType::Data)
    }
    #[test]
    fn can_read_datafile_ending() {
        let file_bytes = read_binary_file("test/B08CB78A.BIN");
        assert_eq!(FILEENDING, file_bytes[file_bytes.len() - 4..]);
    }

    #[test]
    fn can_get_timestamp_of_sample() {
        let file_bytes = read_binary_file("test/B0618B3A.BIN");
        let sample = file_bytes[3..13].to_vec();
        assert_eq!(10, sample.len());
        let time: NaiveDateTime =
            NaiveDateTime::parse_from_str("2021-05-29 20:23:00", "%Y-%m-%d %H:%M:%S").unwrap();
        assert_eq!(time, get_timestamp(sample));
    }

    #[test]
    fn can_read_samples_from_a_whole_file() {
        let file_bytes = read_binary_file("test/B0618B3A.BIN");
        let samples = get_samples_from_file_content(file_bytes);
        // let closure_timestamp_getter = | x: DataSample | -> NaiveDateTime {x.timestamp};
        print!("{:?}", samples.iter());
        assert_eq!(samples.unwrap().len(), 2044);
    }

    #[test]
    fn can_recognize_preemptive_file_endings() {
        let content_bytes = [
            0xE0, 0xC5, 0xEA, 0x03, 0x05, 0x08, 0x09, 0x0E, 0x09, 0x01, 0x00, 0x00, 0x00, 0x08,
            0xF4, 0x00, 0x00, 0x00, 0x08, 0xEF, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF,
        ]
        .to_vec();
        let sample1 = DataSample {
            timestamp: NaiveDateTime::parse_from_str("2008-03-05 09:15:00", "%Y-%m-%d %H:%M:%S")
                .unwrap(),
            //voltage_decivolts: 2305,
            voltage: 230.5,
            //current_milliamps: 0,
            current: 0.0,
            kilo_watt_hours: 0.0,
            powerfactor_percent: 0,
        };
        let sample2 = DataSample {
            timestamp: NaiveDateTime::parse_from_str("2008-03-05 09:16:00", "%Y-%m-%d %H:%M:%S")
                .unwrap(),
            //voltage_decivolts: 2292,
            voltage: 229.2,
            //current_milliamps: 0,
            current: 0.0,
            kilo_watt_hours: 0.0,
            powerfactor_percent: 0,
        };
        let sample3 = DataSample {
            timestamp: NaiveDateTime::parse_from_str("2008-03-05 09:17:00", "%Y-%m-%d %H:%M:%S")
                .unwrap(),
            //voltage_decivolts: 2287,
            voltage: 228.7,
            //current_milliamps: 0,
            current: 0.0,
            kilo_watt_hours: 0.0,
            powerfactor_percent: 0,
        };
        let expected_result = [sample1, sample2, sample3].to_vec();
        let actual_result = get_samples_from_file_content(content_bytes).unwrap();
        println!("{:?}", actual_result);
        println!("{:?}", actual_result.len());
        println!("{:?}", expected_result);
        println!("{:?}", expected_result.len());

        let correct_samples_count = actual_result
            .iter()
            .zip(&expected_result)
            .filter(|(a, b)| a == b)
            .count();
        assert_eq!(correct_samples_count, 3);
    }

    #[test]
    fn can_serialize_data() {
        let sample = DataSample {
            timestamp: NaiveDateTime::parse_from_str("2021-05-29 20:23:00", "%Y-%m-%d %H:%M:%S")
                .unwrap(),
            //voltage_decivolts: 2360,
            voltage: 236.0,
            //current_milliamps: 22,
            current: 0.022,
            kilo_watt_hours: 0.236,
            powerfactor_percent: 39,
        };
        let expected = "{\"timestamp\":\"2021-05-29 20:23:00\",\
                        \"voltage\":\"236\",\
                        \"current\":\"0.022\",\
                        \"power\":\"5.192\",\
                        \"powerfactor\":\"39\"}";
        assert_eq!(expected, _format_sample_json(&sample));
    }


    #[test]
    fn can_serialize_data_to_csv_format() -> Result<(), Box<dyn std::error::Error>> {
        let mut wtr = csv::Writer::from_writer(vec![]);

        wtr.serialize(DataSample {
            timestamp: NaiveDateTime::parse_from_str("2021-05-29 20:23:00", "%Y-%m-%d %H:%M:%S")
                .unwrap(),
            //voltage_decivolts: 2360,
            voltage: 236.0,
            //current_milliamps: 22,
            current: 0.022,
            kilo_watt_hours: 0.2305,
            powerfactor_percent: 39,
        })?;
        wtr.serialize(DataSample {
            timestamp: NaiveDateTime::parse_from_str("2021-05-29 20:24:00", "%Y-%m-%d %H:%M:%S")
                .unwrap(),
            //voltage_decivolts: 260,
            voltage: 23.0,
            //current_milliamps: 2,
            current: 0.02,
            kilo_watt_hours: 0.2305,
            powerfactor_percent: 120,
        })?;

        let data = String::from_utf8(wtr.into_inner()?)?;

        assert_eq!(
            data,
            "\
timestamp,voltage,current,kilo_watt_hours,powerfactor_percent
2021-05-29T20:23:00,236.0,0.022,0.2305,39
2021-05-29T20:24:00,23.0,0.02,0.2305,120
"
        );
        Ok(())
    }

}